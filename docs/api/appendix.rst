Appendix
========

In the following you can find additional details regarding the KORD api.

Alarm States Encoding
---------------------

The `systemAlarmState()` method provides the condensed state of the present error state of the RC. In case the robot controller is suspended due the ESTOP, PSTOP 
or any other kind of error, the function returns non-zero value that encodes the necessary information about this recent state. It consists of following:

- Category (Safety Event, Soft Stop, Hardware Status). Indicates the content of the condition identifier.
- Context represents a brief snapshot of other collateral alarm states present in the system. E.g. when the system was suspended due to some PSTOP,
  but as a consequence ended up in the ESTOP, both ESTOP|PSTOP flags should be contained.
- Condition, keeps the specific error identification.

In the following tables you find the useful condition identifier based on category.

Safety Event Conditions
~~~~~~~~~~~~~~~~~~~~~~~

+-------------------------------+------------+-------------------------------------------------+
| SAFETY EVENT Condition ID     | Value      | Description                                     |
|                               |            |                                                 |
+===============================+============+=================================================+
| EXTERNAL_ESTOP_ACTIVATED      | 1001       | EStop signal was activated by the IOBoard.      |
+-------------------------------+------------+-------------------------------------------------+
| IOB_NOT_RESPONDIN             | 1002       | IOBoard is not responding in a row of 5 updates.|
+-------------------------------+------------+-------------------------------------------------+
| JBS_NOT_RESPONDING            | 1003       | JBoards are not responding in a row of 5        |
|                               |            | updates.                                        |
+-------------------------------+------------+-------------------------------------------------+
| JREF_X_SENSOR_POSITION_SPAN   | 1004       | Detected span of sensor and reference data      |
|                               |            | exceeding rated tolerances.                     |
+-------------------------------+------------+-------------------------------------------------+
| JREF_POSITION_DELTA_SPAN      | 1005       | Joint Reference position step in single update  |
|                               |            | exceeds rated tolerance.                        |
+-------------------------------+------------+-------------------------------------------------+
| JRATED_SPEED_EXCEEDED         | 1006       | Rated joint speed exceeded (sensors).           |
+-------------------------------+------------+-------------------------------------------------+
| JRATED_TORQUE_EXCEEDED        | 1007       | Rated torque exceeded (sensors).                |
+-------------------------------+------------+-------------------------------------------------+
| JHOLD_TORQUE_EXCEEDED         | 1008       | Hold torque limit exceeded.                     |
+-------------------------------+------------+-------------------------------------------------+
| JBRATED_TEMP_EXCEEDED         | 1009       | Joint board rated temperature was exceeded.     |
+-------------------------------+------------+-------------------------------------------------+
| JTORQUE_DEVIATION_EXCEEDED    | 1010       | Joint max torque deviation was exceeded.        |
+-------------------------------+------------+-------------------------------------------------+
| MODEL_X_TRJ_REFJ_SSPAN_EXC    | 1011       | Model/reference position span exceeding         |
|                               |            | prescribed limits in joint axis coordinates.    |
+-------------------------------+------------+-------------------------------------------------+
| MODEL_X_TRJ_REFW_SSPAN_EXC    | 1012       | Model/reference position span exceeding         |
|                               |            | prescribed limits in the context                |
|                               |            | of operational space.                           |
+-------------------------------+------------+-------------------------------------------------+
| FRAME_SPEED_LIMIT_EXC         | 1013       | Robot frames exceeded rated speed limits.       |
+-------------------------------+------------+-------------------------------------------------+
| EXTERNAL_PSTOP_ACTIVATED      | 2001       | PStop signal was activated by the IOBard.       |
+-------------------------------+------------+-------------------------------------------------+

Soft Stop Conditions
~~~~~~~~~~~~~~~~~~~~

+---------------------------------+------------+-------------------------------------------------+
| SOFT STOP Condition ID          | Value      | Description                                     |
|                                 |            |                                                 |
+=================================+============+=================================================+
| MODEL_INVALID_STATE             | 2001       | Invalid internal state was detected.            |
+---------------------------------+------------+-------------------------------------------------+
| MODEL_JVELOCITY_LIMITS_EXC      | 2002       | Soft stop initiated after the model joints speed|
|                                 |            | exceeded internal limits.                       |
+---------------------------------+------------+-------------------------------------------------+
| MODEL_JTORQUE_LIMITS_EXC        | 2003       | Soft stop initiated after the model feed        |
|                                 |            | forward (reference) torques exceeded internal   |
|                                 |            | limits.                                         |
+---------------------------------+------------+-------------------------------------------------+
| MODEL_JSDTORQUE_LIMITS_EXC      | 2003       | Static torques exceeded internal limits.        |
+---------------------------------+------------+-------------------------------------------------+
| MODEL_JPOS_LIMITS_VIOLATION_EST | 2003       | Preventive soft stop triggered in case joints   |
|                                 |            | approach the limit based on extrapolated        |
|                                 |            | estimation                                      |
+---------------------------------+------------+-------------------------------------------------+


HW Stat Conditions
~~~~~~~~~~~~~~~~~~
+---------------------------------+------------+-------------------------------------------------+
| HW STAT flags                   | Value      | Description                                     |
|                                 |            |                                                 |
+=================================+============+=================================================+
| HW_STAT_LOW_VOLTAGE_AFTER       | 0x01       | Power relay disfunct error status.              |
| _RELAY_DETECTED                 |            |                                                 |
+---------------------------------+------------+-------------------------------------------------+
| HW_STAT_INIT_BLOCKED            | 0x02       | Robot initialization is interrupted,            |
|                                 |            | user action is required.                        |                                    
+---------------------------------+------------+-------------------------------------------------+
| HW_STAT_INIT_RUID_MISMATCH      | 0x04       | New robot identification requires user action.  |
|                                 |            | This state occurs after the new robot arm       |
|                                 |            | is connected with the RC for the first time.    |
+---------------------------------+------------+-------------------------------------------------+
| HW_STAT_HARD_FAULT              | 0x08       | Internal hardware failure, complete RC restart  |
|                                 |            | is required.                                    |
+---------------------------------+------------+-------------------------------------------------+
