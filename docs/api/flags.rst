
.. _flags:

Flags
-----

kord::protocol::EMotionFlags
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenenum:: kr2::kord::protocol::EMotionFlags

kord::protocol::ESafetyFlags
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenenum:: kr2::kord::protocol::ESafetyFlags

kord::protocol::ESafetyMode
~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenenum:: kr2::kord::protocol::ESafetyMode

kord::protocol::EHWFlags
~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenenum:: kr2::kord::protocol::EHWFlags

kord::protocol::EButtonFlags
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenenum:: kr2::kord::protocol::EButtonFlags

kord::protocol::ESystemAlarmCategory
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenenum:: kr2::kord::protocol::ESystemAlarmCategory

kord::protocol::ESystemAlarmContext
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenenum:: kr2::kord::protocol::ESystemAlarmContext
