#include <iostream>
#include <iomanip>
#include <cmath>

#include <kord/api/kord_receive_interface.h>
#include <kord/api/kord_control_interface.h>
#include <kord/api/kord.h>
#include <kord/utils/utils.h>

#include <csignal>
#include <chrono>
#include <sstream>

using namespace kr2;
static bool g_run = true;

void signal_handler( int a_signum ) {
    psignal(a_signum, "[KORD-API]");
    g_run = false;
}

int main(int argc, char * argv[])
{
    kr2::utils::LaunchParameters lp = kr2::utils::LaunchParameters::processLaunchArguments(argc, argv);

    if (lp.help_ || !lp.valid_) {
        //lp.printUsage(true);
        return EXIT_SUCCESS;
    }

    signal(SIGINT, signal_handler); 

    if (lp.useRealtime()){
        if (!kr2::utils::realtime::init_realtime_params(lp.rt_prio_)){
            std::cerr << "Failed to start with realtime priority\n";
            lp.printUsage(true);
            return EXIT_FAILURE;
        }
    }
    
    std::cout << "Connecting to: " << lp.remote_controller_ << ":" << lp.port_ << "\n";
    std::cout << "[KORD-API] Session ID: " << lp.session_id_ << std::endl;

    std::shared_ptr<kord::KordCore> kord(new kord::KordCore(
        lp.remote_controller_,
        lp.port_,
        lp.session_id_,
        kord::UDP_CLIENT));


    kord::ControlInterface ctl_iface(kord);
    kord::ReceiverInterface rcv_iface(kord);


    if (!kord->connect()) {
        std::cout << "Connecting to KR failed\n";
        return EXIT_FAILURE;
    }

    std::array<double, 7UL> q, qd, qdd, torque;
    
    g_run = true;
    
    // Obtain initial q values
    if (!kord->syncRC()){
        std::cout << "Sync RC failed.\n";
        return EXIT_FAILURE;
    }
    std::cout << "Sync Captured \n";
    rcv_iface.fetchData();
    std::array<double, 7UL> start_q = rcv_iface.getJoint(kord::ReceiverInterface::EJointValue::S_ACTUAL_Q);

    std::cout << "Read initial joint configuration:\n";
    for( double angl: start_q )
        std::cout << (angl/3.14)*180 << " ";

    std::cout << "\n";

    unsigned int i = 0;
    double a       = 0.1;
    double t       = 0.0;

    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

    while(g_run) {
        
        for (size_t i = 0; i<7; i++) 
        {
            double b = i * 1.0e-3;
            q  [i]    =  a*std::cos(t * b) - a + start_q[i];
            qd [i]    = -a*b*std::sin(t * b);
            qdd[i]    = -a*b*b*std::cos(t * b);
            torque[i] = 0.0; // it will be computed automatically in case of zero
        }
        // torque = {-3.0, -16., 0.15, 11.2, 0.7, -0.36, 0.5}; // some torque for testing
        t = i * 7;
        i++;

        if (!kord->waitSync(std::chrono::milliseconds(10)))
        {
            std::cout << "Sync wait timed out, exit \n";
            break;
        }

        ctl_iface.directJControl(q, qd, qdd, torque);
        
        rcv_iface.fetchData();
        if (rcv_iface.systemAlarmState()  ||
            lp.runtimeElapsed())
        { break; }
    }

    std::cout << "Robot stopped\n";
    std::cout << rcv_iface.getFormattedInputBits()  << std::endl;
    std::cout << rcv_iface.getFormattedOutputBits() << std::endl;
    std::cout << "SystemAlarmState:\n";
    std::cout << rcv_iface.systemAlarmState() << "\n";
    std::cout << "SystemAlarmState's Category:\n";
    switch (rcv_iface.systemAlarmState() & 0b1111){
        case kr2::kord::protocol::EEventGroup::eUnknown:
            std::cout << "No alarms" << '\n';
            break;
        case kr2::kord::protocol::EEventGroup::eSafetyEvent:
            std::cout << "Safety Event" << '\n';
            break;
        case kr2::kord::protocol::EEventGroup::eSoftStopEvent:
            std::cout << "Soft Stop Event" << '\n';
            break;
        case kr2::kord::protocol::EEventGroup::eKordEvent:
            std::cout << "Kord Event" << '\n';
            break;
    }

    std::cout << "Safety flags: " << rcv_iface.getRobotSafetyFlags() << "\n";
    std::cout << "Runtime: " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start).count()/1000.0 << " [s]\n";
    std::cout << "SafetyFlags: " << rcv_iface.getRobotSafetyFlags() << "\n";
    std::cout << "MotionFlags: " << rcv_iface.getMotionFlags() << "\n";
    std::cout << "RCState: " << rcv_iface.getRobotSafetyFlags() << "\n";
    
    kord->printStats(rcv_iface.getStatisticsStructure());
    return 0;
}
