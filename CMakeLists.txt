cmake_minimum_required(VERSION 3.5)

set(CMAKE_VERBOSE_MAKEFILE OFF)
set(CMAKE_CXX_STANDARD 17)

project(kord_remote_control)

#set(BOOST_ALL_NO_LIB ON)

# Disable Boost
#
#set(Boost_NO_BOOST_CMAKE OFF)

#set(Boost_USE_STATIC_LIBS        OFF)  # only find static libs
#set(Boost_USE_DEBUG_LIBS        OFF)  # ignore debug libs and
#set(Boost_USE_RELEASE_LIBS       OFF)  # only find release libs
#set(Boost_USE_MULTITHREADED      OFF)
#set(Boost_USE_STATIC_RUNTIME    OFF)
#find_package(Boost REQUIRED thread system)
#
# Disable Boost End

find_package(Threads REQUIRED)

include_directories(
    # ${Boost_INCLUDE_DIR} 
    external/kord-protocol/include
    external
    external/asio
)

add_subdirectory(external/kord-protocol)

#
# NOTE: temporarily commented out for the master branch
#add_subdirectory(kord_test)

add_library(kord SHARED
    src/api/kord.cpp
    src/api/connection_interface.cpp
    src/api/kord_control_interface.cpp
    src/api/kord_receive_interface.cpp
    src/api/kord_io_request.cpp
    src/utils/timex.cpp
    src/utils/utils.cpp
)

target_include_directories(
    kord
    PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:include>
)

add_executable(ex_move_joints
    examples/ex_move_joints.cpp
)

add_executable(ex_move_linear
    examples/ex_move_linear.cpp
)

add_executable(ex_move_velocity
    examples/ex_move_velocity.cpp
)

add_executable(ex_read_state
    examples/ex_read_state.cpp
)

add_executable(ex_brake_control
    examples/ex_brake_control.cpp
)

add_executable(ex_transfer_logs
    examples/ex_transfer_logs.cpp
)

add_executable(
    ex_retrieve_errors
    examples/ex_retrieve_errors.cpp
)

add_executable(ex_move_direct
    examples/ex_move_direct.cpp
)

add_executable(ex_move_joints_discrete
    examples/ex_move_joints_discrete.cpp
)

add_executable(ex_move_linear_discrete
    examples/ex_move_linear_discrete.cpp
)

add_executable(ex_set_oport
    examples/ex_set_oport.cpp
)


add_executable(ex_read_temperature
    examples/ex_read_temperature.cpp
)


add_executable(ex_set_frame
    examples/ex_set_frame.cpp
)


add_executable(ex_set_load
    examples/ex_set_load.cpp
)

add_executable(ex_get_tcp_and_co
    examples/ex_get_tcp_and_co.cpp
)

add_executable(ex_clean_alarm
    examples/ex_clean_alarm.cpp
)


add_executable(ex_transfer_json
    examples/ex_transfer_json.cpp
)

add_executable(ex_transfer_calibration_data
    examples/ex_transfer_calibration_data.cpp
)

add_executable(ex_transfer_more_files
    examples/ex_transfer_more_files.cpp
)

#link_directories(${Boost_LIBRARIES})
target_link_libraries(kord kord_protocol)
target_link_libraries(ex_move_joints kord pthread)
target_link_libraries(ex_move_linear kord pthread)
target_link_libraries(ex_move_direct kord pthread)
target_link_libraries(ex_move_joints_discrete kord pthread)
target_link_libraries(ex_move_linear_discrete kord pthread)
target_link_libraries(ex_move_velocity kord pthread)
target_link_libraries(ex_read_state kord pthread)
target_link_libraries(ex_brake_control kord pthread)
target_link_libraries(ex_transfer_logs kord)
target_link_libraries(ex_retrieve_errors kord)
target_link_libraries(ex_transfer_json kord)
target_link_libraries(ex_transfer_calibration_data kord)
target_link_libraries(ex_transfer_more_files kord)

target_link_libraries(ex_set_oport kord)

target_link_libraries(ex_read_temperature kord pthread )
target_link_libraries(ex_set_frame kord pthread)
target_link_libraries(ex_set_load kord pthread)
target_link_libraries(ex_get_tcp_and_co kord pthread)
target_link_libraries(ex_clean_alarm kord pthread )

set_property(
    TARGET
    kord
    ex_move_joints
    ex_move_linear
    ex_move_velocity
    ex_read_state
    ex_read_temperature
    ex_brake_control
    ex_set_frame
    ex_set_load
    ex_get_tcp_and_co
    ex_clean_alarm
    ex_transfer_json
    ex_transfer_calibration_data
    ex_transfer_more_files
    PROPERTY CXX_STANDARD 17
)
