/*////////////////////////////////////////////////////////////////////////////
//
// (C) Copyright 2022 by Kassow Robots, ApS
//
// The information contained herein is confidential, proprietary to Kassow Robots,
// ApS, and considered a trade secret as defined in section 263 and section 264
// under the Danish Criminal Code. Use of this information by anyone
// other than authorized employees of Kassow Robots, ApS is granted only under a
// written non-disclosure agreement, expressly prescribing the scope and
// manner of such use.
//
// Authors: Alexander Kazakov aka@kassowrobots.com
//
/////////////////////////////////////////////////////////////////////////////*/

#include "kord/api/kord_io_request.h"

namespace kr2 {
namespace kord {

namespace kkp = kr2::kord::protocol;

RequestIO::RequestIO() { request_type_ = kkp::EKORDItemID::eCommandSetIODigitalOut; };
RequestIO::RequestIO(const Request&a_req): Request(a_req) { };

RequestIO& RequestIO::asSetIODigitalOut()
{
    this->system_request_type_ = kkp::EControlCommandItems::eIOSet;
    this->request_rid_         = std::chrono::steady_clock::now().time_since_epoch().count();
    this->request_type_        = kkp::EKORDItemID::eCommandSetIODigitalOut;
    return *this;
}

// RequestIO& RequestIO::asSetIOAnalogOut()
// {
//     this->system_request_type_ = kkp::EControlCommandItems::eIOSet;
//     this->request_rid_         = std::chrono::high_resolution_clock::now().time_since_epoch().count();
//     this->request_type_        = kkp::EKORDItemID::eCommandSetIOAnalogOut;
//     return *this;
// }

RequestIO& RequestIO::withEnabledPorts(int64_t v)
{
    this->value_ = 1;
    this->mask_ = (int64_t)v;
    return *this;
}

RequestIO& RequestIO::withDisabledPorts(int64_t v)
{
    this->value_ = 0;
    this->mask_ = (int64_t)v;
    return *this;
}

}
}