//
//  main.cpp
//  - kord_prototype
//  - brake test
//
//

#include <iostream>
#include <iomanip>
#include <cmath>

#include <kord/api/kord_receive_interface.h>
#include <kord/api/kord_control_interface.h>
#include <kord/api/kord.h>
#include <kord/utils/utils.h>

#include <csignal>
#include <chrono>
#include <sstream>

using namespace kr2;
static bool g_run = true;

void signal_handler( int a_signum ) {
    psignal(a_signum, "[KORD-API]");
    g_run = false;
}

int main(int argc, char * argv[])
{
    struct ExtraOptions {
        bool engage_mode = true;
    } extras;
    static kr2::utils::LaunchParameters::ExternalArgParser ep = [argc, &argv, &extras](int index)->void {
    static kr2::utils::SOALongOptions ex_options{std::array<kr2::utils::LongOption, 2>{
        kr2::utils::LongOption{{  "engage",  no_argument, nullptr,  'e' }, "lock joints"},
        kr2::utils::LongOption{{  "disengage", no_argument, nullptr,  'd' }, "unlock joints"}
    }};
    
    if (index <= kr2::utils::LaunchParameters::INVALID_INDEX) {
        std::cout << ex_options.helpString() << "\n";
        return;
    }

    int option_index = 0;
    optind = index;
    int opt = getopt_long(argc, argv, "ed", ex_options.getLongOptions(), &option_index);

    switch (opt) {
        case 'e':
            extras.engage_mode = true;
            break;
        case 'd':
            extras.engage_mode = false;
            break;
        default:
            std::cout << "Unknown option found" << " optidx: " << optind << ", argc: " << argc << "\n";
            exit(EXIT_FAILURE);
            break;
    }
};
    kr2::utils::LaunchParameters lp = kr2::utils::LaunchParameters::processLaunchArguments(argc, argv, ep);
    
    if (lp.help_ || !lp.valid_) {
        return EXIT_SUCCESS;
    }

    signal(SIGINT, signal_handler); 

    if (lp.useRealtime()){
        if (!kr2::utils::realtime::init_realtime_params(lp.rt_prio_)){
            std::cerr << "Failed to start with realtime priority\n";
            lp.printUsage(false);
            return EXIT_FAILURE;
        }
    }
    
    std::cout << "Connecting to: " << lp.remote_controller_ << ":" << lp.port_ << "\n";
    std::cout << "[KORD-API] Session ID: " << lp.session_id_ << std::endl;

    std::shared_ptr<kord::KordCore> kord(new kord::KordCore(
        lp.remote_controller_,
        lp.port_,
        lp.session_id_,
        kord::UDP_CLIENT));


    kord::ControlInterface ctl_iface(kord);
    kord::ReceiverInterface rcv_iface(kord);


    if (!kord->connect()) {
        std::cout << "Connecting to KR failed\n";
        return EXIT_FAILURE;
    }

    g_run = true;
    
    
    // Obtain initial q values
    if (!kord->syncRC()){
        std::cout << "Sync RC failed.\n";
        return EXIT_FAILURE;
    }
    std::cout << "Sync Captured \n";
    rcv_iface.fetchData();
    std::array<double, 7UL> start_q = rcv_iface.getJoint(kord::ReceiverInterface::EJointValue::S_ACTUAL_Q);

    std::cout << "Read initial joint configuration:\n";
    for( double angl: start_q )
        std::cout << (angl/3.14)*180 << " ";

    std::cout << "\n";

    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

    while(g_run) {

        //...insert code here
        //

        // Wait for the hearbeat
        if (!kord->waitSync(std::chrono::milliseconds(10))){
            std::cout << "Sync wait timed out, exit \n";
            break;
        }

        // When heartbeat was captured, transmit the request - brake joints 5,6,7
        std::vector<int> joints{5,6,7};
        int64_t token;
        if (extras.engage_mode){
            std::cout << "Engaging brakes...\n";
            ctl_iface.engageBrakes(joints, token);
        }
        else{
            std::cout << "Disengaging brakes...\n";
            ctl_iface.disengageBrakes(joints, token);
        }

        if (!kord->waitSync(std::chrono::milliseconds(20))){
            std::cout << "Sync wait timed out, exit \n";
            break;
        }

        std::cout << "Command sent \n";
        std::cout << "Command token: " << token << '\n';
        while(rcv_iface.getCommandStatus(token) == -1){
            if (g_run == false){
                break;
            }
            std::cout << "not found yet \n";
            if (!kord->waitSync(std::chrono::milliseconds(20))){
                std::cout << "Sync wait timed out, exit \n";
                break;
            }
            
            rcv_iface.fetchData();
            //std::cout << "1";
        }
        int status = rcv_iface.getCommandStatus(token);
        std::cout << '\n' << "Command status: " << status << '\n';

        // Currently there is no feedback to check whether the brakes were really engaged.

        break;
    }

    std::cout << "Brake command sent\n";

    return 0;
}
