

#include <iostream>
#include <iomanip>
#include <cmath>

#include <kord/api/kord_receive_interface.h>
#include <kord/api/kord_control_interface.h>
#include <kord/api/kord.h>
#include <kord/utils/utils.h>

#include <csignal>
#include <chrono>
#include <sstream>

using namespace kr2;
volatile bool stop = false;

void signal_handler( sig_atomic_t a_signum ) {
    psignal(a_signum, "[KORD-API]");
    stop = true;
}

namespace kr2 {
namespace ex {

enum EClearCommand : int {
    NONE = 0,
    CLEAR_HALT = 1001,
    UNSUSPEND = 1002,
    CONTINUE_INIT = 1003,
    CLEAR_CBUN = 1004
};

} // namespace ex(amples)
} // namespace kr2


int main(int argc, char * argv[])
{
    struct ExtraOptions {
        double runtime = 0.0;
        kord::ControlInterface::EClearRequest command_to_RC = kord::ControlInterface::EClearRequest::CLEAR_HALT;
    } extras;
    static kr2::utils::LaunchParameters::ExternalArgParser ep = [argc, &argv, &extras](int index)->void {
    static kr2::utils::SOALongOptions ex_options{std::array<kr2::utils::LongOption, 5>{
        kr2::utils::LongOption{{  "runtime",  required_argument, nullptr,  't' }, "for how long the process should keep running"},
        kr2::utils::LongOption{{     "halt",        no_argument, nullptr,  'h' }, "clear halt on the controller"},
        kr2::utils::LongOption{{"unsuspend",        no_argument, nullptr,  'u' }, "unsuspend the robot"},
        kr2::utils::LongOption{{     "init",        no_argument, nullptr,  'i' }, "continue robot initialization in case it was blocked"},
        kr2::utils::LongOption{{     "cbun",        no_argument, nullptr,  'c' }, "acknowledge CBun error and clear it"}
    }};
    
    if (index <= kr2::utils::LaunchParameters::INVALID_INDEX) {
        std::cout << ex_options.helpString() << "\n";
        return;
    }

    int option_index = 0;
    optind = index;
    int opt = getopt_long(argc, argv, "x:yzuv", ex_options.getLongOptions(), &option_index);

    switch (opt) {
        case  't':
            extras.runtime = std::stod(optarg);
            break;
        case 'h':
            extras.command_to_RC = kord::ControlInterface::EClearRequest::CLEAR_HALT;
            break;
        case 'u':
            extras.command_to_RC = kord::ControlInterface::EClearRequest::UNSUSPEND;
            break;
        case 'i':
            extras.command_to_RC = kord::ControlInterface::EClearRequest::CONTINUE_INIT;
            break;
        case 'c':
            extras.command_to_RC = kord::ControlInterface::EClearRequest::CBUN_EVENT;
            break;
        default:
            std::cout << "Unknown option found" << " optidx: " << optind << ", argc: " << argc << "\n";
            exit(EXIT_FAILURE);
            break;
    }
};


    kr2::utils::LaunchParameters lp = kr2::utils::LaunchParameters::processLaunchArguments(argc, argv, ep);

    if (lp.help_ || !lp.valid_) {
        return EXIT_SUCCESS;
    }

    signal(SIGINT, signal_handler);

    if (lp.useRealtime()){
        if (!kr2::utils::realtime::init_realtime_params(lp.rt_prio_)){
            std::cerr << "Failed to start with realtime priority\n";
            lp.printUsage(false);
            return EXIT_FAILURE;
        }
    }

    std::cout << "Connecting to: " << lp.remote_controller_ << ":" << lp.port_ << "\n";
    std::cout << "[KORD-API] Session ID: " << lp.session_id_ << std::endl;

    std::shared_ptr<kord::KordCore> kord(new kord::KordCore(
        lp.remote_controller_,
        lp.port_,
        lp.session_id_,
        kord::UDP_CLIENT));
    
    // insert code here...
    kord::ControlInterface ctl_iface(kord);
    kord::ReceiverInterface rcv_iface(kord);

    if (!kord->connect()) {
        std::cout << "Connecting to KR failed\n";
        return EXIT_FAILURE;
    }
    bool run = true;
    
    if (!kord->syncRC()){
        std::cout << "Sync RC failed.\n";
        return EXIT_FAILURE;
    }
    std::cout << "Sync Captured \n";

    int64_t token = ctl_iface.clearAlarmRequest(extras.command_to_RC);
    //token = ctl_iface.clearAlarmRequest(kord::ControlInterface::EClearRequest::CBUN_EVENT);

    std::cout << "Command sent \n";
    std::cout << "Command token: " << token << '\n';
    while(rcv_iface.getCommandStatus(token) == -1){
        std::cout << "not found yet \n";
        if (!kord->waitSync(std::chrono::milliseconds(10), kord::F_SYNC_FULL_ROTATION)){
            std::cout << "Sync wait timed out, exit \n";
            break;
        }

        rcv_iface.fetchData();
        //std::cout << "1";
        if (stop){
            break;
        }
    }
    int status = rcv_iface.getCommandStatus(token);
    std::cout << '\n' << "Found the command status: " << status << '\n';

    return 0;
}
