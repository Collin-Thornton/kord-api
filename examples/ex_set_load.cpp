

#include <iostream>
#include <iomanip>
#include <cmath>

#include <kord/api/kord_receive_interface.h>
#include <kord/api/kord_control_interface.h>
#include <kord/api/kord.h>
#include <kord/utils/utils.h>

#include <csignal>
#include <chrono>
#include <sstream>

using namespace kr2;
volatile bool stop = false;

void signal_handler( sig_atomic_t a_signum ) {
    psignal(a_signum, "[KORD-API]");
    stop = true;
}
int main(int argc, char * argv[])
{
    kr2::utils::LaunchParameters lp = kr2::utils::LaunchParameters::processLaunchArguments(argc, argv);

    if (lp.help_ || !lp.valid_) {
        //lp.printUsage(false);
        return EXIT_SUCCESS;
    }

    signal(SIGINT, signal_handler);

    if (lp.useRealtime()){
        if (!kr2::utils::realtime::init_realtime_params(lp.rt_prio_)){
            std::cerr << "Failed to start with realtime priority\n";
            lp.printUsage(false);
            return EXIT_FAILURE;
        }
    }

    std::cout << "Connecting to: " << lp.remote_controller_ << ":" << lp.port_ << "\n";
    std::cout << "[KORD-API] Session ID: " << lp.session_id_ << std::endl;

    std::shared_ptr<kord::KordCore> kord(new kord::KordCore(
        lp.remote_controller_,
        lp.port_,
        lp.session_id_,
        kord::UDP_CLIENT));
    
    // insert code here...
    kord::ControlInterface ctl_iface(kord);
    kord::ReceiverInterface rcv_iface(kord);

    if (!kord->connect()) {
        std::cout << "Connecting to KR failed\n";
        return EXIT_FAILURE;
    }

    bool run = true;
    
    if (!kord->syncRC()){
        std::cout << "Sync RC failed.\n";
        return EXIT_FAILURE;
    }
    std::cout << "Sync Captured \n";

    std::array<double, 3UL> p = {0.0011, 0.0022, 0.0033};
    std::array<double, 6UL> in = {0.01, 0.02, 0.03, 0.004 ,0.005 ,0.006};
    
    //std::array<double, 3UL> p = {0.0, 0.0, 0.0};
    //std::array<double, 6UL> in = {0.0, 0.0, 0.0, 0.0,0.0,0.0};
    
    int64_t token;
    ctl_iface.setLoad(kord::ELoadID::LOAD1, 0.0, p, in, token);

    std::cout << "Command sent \n";
    std::cout << "Command token: " << token << '\n';
    while(rcv_iface.getCommandStatus(token) == -1){
        std::cout << "not found yet \n";
        if (!kord->waitSync(std::chrono::milliseconds(20))){
            std::cout << "Sync wait timed out, exit \n";
            break;
        }
        

        rcv_iface.fetchData();
        //std::cout << "1";
    }
    int status = rcv_iface.getCommandStatus(token);
    std::cout << '\n' << "Command status: " << status << '\n';

    return 0;
}
