.. KORD Api documentation master file, created by
   sphinx-quickstart on Wed Oct 26 01:37:01 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to KORD Api's documentation!
====================================
Version 1.4.1

**KORD** is the Kassow Robots real-time protocol designed for the external controller communication 
with the KR robot controllers.

.. note::

   This project is under active development.

.. toctree::
   :caption: Content
   :maxdepth: 2

   Home <self>
   installation/installation
   introduction/introduction
   api/api
   statistics/statistics
   statistics/kord_ini
   examples/examples
   faq/faq

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
